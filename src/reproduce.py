#!/usr/bin/env python3

import csv
import numpy as np
import matplotlib.pyplot as plt
from itertools import groupby

# resulting files are stored under this path
results_path = "/home/fherrmann/Dokumente/Conquaire/ReX-chem-koop-binary/results/"

# path to the Snomax data file
data_path = "/home/fherrmann/Dokumente/Conquaire/ReX-chem-koop-binary/Snomax.txt"


# bins a given float number to a float with one decimal
def bin_value(val):
    # save sign of number
    if val < 0: sign = -1
    else: sign = 1
    # get decimal part of float    
    first_two_decimals = str(abs(val) % 1)
    # calculate binned float
    binned_val = 0
    if (int(first_two_decimals[3:]) > 0):
        binned_dec = float(first_two_decimals[0:3]) + 0.1
        binned_val = sign * (int(abs(val)) + binned_dec)
    return binned_val

# calculate the calibrated temperature from given t_nuk with the cooling rate r
def calc_t_cal(t_nuk, r):
    t_cal = -1.0 * ((-1.0*(6.03165)) + 0.02113 * (273.15 + t_nuk)) - (3.59774 + (-1.0*(0.02956)) * (273.15 + t_nuk) + 6.10156 * (1.0 / np.power(10, 5)) * np.power((273.15 + t_nuk), 2)) * (-1.0 * r) + t_nuk

    # bin the given value
    bin_t_cal = bin_value(t_cal)
    return bin_t_cal

# calculate f_ice for each concentration bin
def calc_f_ice(n_t_cal, n_total):
    f_ice = n_t_cal / n_total
    return f_ice

# calculate n_m for each concentration bin
def calc_n_m(f_ice, c, v_drop):
    if f_ice < 1:
        n_m = (-1*(np.log(1 - f_ice))) / (c * v_drop)
        return n_m 
    else:
        return "undefined"

# store resulting table after STEP 6 as a csv file
def storeCalculatedData(data, results_path):
    writer = csv.writer(open (results_path + "data_fig4.csv", 'w'), delimiter=',', lineterminator='\n')
    writer.writerow(["T_cal [°C]", "#events", "n(T_cal)", "f_ice(T_cal)", "n_m(T_cal)", "concentration [mg/mL]"])
    for i, conc in enumerate(data):
        for c in conc:
            writer.writerow(c)        

# n_m is plotted versus T_cal for all concetrations. Like in the paper the color indicates the concentration and the thickness indicates the number of events for a temperature event.
def plot_results(data, concentrations, results_path):
    #concentrations = [1.0, 0.1, 0.01, 0.001, 0.0001, 2.6e-05, 1e-05, 4e-06, 1e-06]
    colors = ["cyan", "blue", "green", "lime", "yellow", "orange", "red", "magenta", "pink"]

    fig, ax = plt.subplots()
    for i, val in enumerate(data):
        for e in val:
            #print(e)
            if e[4] != "undefined":
                ax.scatter(float(e[0]), float(e[4]), c = colors[i], s=e[2], alpha=0.5)

    # scale x axis like figure in the paper
    ax.set_xlim(-12, -2)

    # scale y axis like figure in the paper
    ax.set_yscale("log")
    ax.set_yticks(np.logspace(-3, 7, num=11, base=10.0, dtype='float'))

    # set axis titles
    ax.set_xlabel(r"$T$ $[°C]$")
    ax.set_ylabel(r"$n_{m}$ $[\mu g^{-1}]$")
    ax.set_title("Figure 4")

    # reconfigure legend displayed in the plot
    # legend for different circle sizes which shows number of events of a temperature
    n_size = [1, 5, 10, 15, 20, 30]
    legend1 = ax.legend(n_size, title=r"$\Delta n_{ice}$", loc=[0.22, 0.02])
    [legend1.legendHandles[i].set_color(["grey"]) for i,_ in enumerate(n_size)]
    ax.add_artist(legend1)
    # legend for different concentrations
    ax.legend(concentrations[::-1], title=r"c $[\mu g $ $drop^{-1}]$") # reverse order of legend
    leg = ax.get_legend()
    for i, _ in enumerate(leg.legendHandles):
        leg.legendHandles[i].set_color(colors[-(i+1)])
        leg.legendHandles[i].set_sizes([100])

    # create a grid in the output plot
    ax.grid()
    fig.tight_layout()

    # save figure as pdf and show it
    plt.savefig(results_path + "fig4.pdf", format="pdf")
    plt.show()


# main function which reproduces the results
def main():
    # read in file
    input = ""
    with open(data_path, 'rb') as f:
        input = str(f.read())

    # remove unnessesary characters
    row = input.strip("b'").split("\\r\\n")

    # create array from input data
    arr = []
    for entry in row:
        split_row = entry.split("\\t")
        arr.append(split_row)

    # remove last element because it is empty because of the split
    if len(arr) > 0:
        del arr[-1] 

    # seperate data from input file because header is stored in the first three lines
    data = arr[3:]

    # extract t_nuk and cooling_rate because the are needed for calculation of t_cal
    t_nuk_col = [float(e[3]) for e in data]
    cooling_rate_col = [int(e[5]) for e in data]

    ### STEP 2: calculate calibrated temperature t_cal ###
    if len(t_nuk_col) == len(cooling_rate_col):
        t_cal = [calc_t_cal(t_nuk_col[i], cooling_rate_col[i]) for i in range(len(t_nuk_col))]

    # replace t_nuk with t_cal and create data
    for i, entry in enumerate(data):
        entry[2] = float(entry[2]) # convert concentration from string to float
        entry[3] = t_cal[i]

    ### STEP 3: Sort all columns by concentration and within each concentration bin by temperature t_cal ###
    # create a set containing all different concetrations
    concentrations = sorted(set([d[2] for d in data]), reverse=True)

    # create groups for each concentration
    conc_groups = [[d for d in data if d[2] == c] for c in concentrations]

    # list which contains for each concentration the temperature, the number of events, n_t_cal, f_ice and n_m
    conc_t_events = []

    # sort each group after t_cal value
    for index, values in enumerate(conc_groups):
        values.sort(key=lambda x:x[3], reverse=True)

        v_drop = 0
        for v in values:
            v_drop += float(v[4])
        v_drop = v_drop/len(values)

        # list which contains each temperature and the number of its events
        t_events = []

        # group the data after t_cal and count the events
        groups = groupby(values, lambda x:x[3])
        for key, val in groups:
            i = 0
            for v in val: 
                i += 1
            t_events.append([key, i])

        ### STEP 4/5: Determination of f_ice and n_m for each concentration bin ###
        # calc n(t_cal) and f_ice and append this values to each tuple
        for i, t in enumerate(t_events):
            if i == 0:
                n_t_cal = t[1]
            else:
                n_t_cal = t_events[i-1][2] + t[1]
            t.append(n_t_cal)
            t.append(calc_f_ice(n_t_cal, len(values)))
            t.append(calc_n_m(t[-1], concentrations[index], v_drop))
            t.append(concentrations[index]) # the concentration is also added for each bin to be able to seperate the output data

        conc_t_events.append(t_events)    


    ### store data in a csv ###
    storeCalculatedData(conc_t_events, results_path)


    ### STEP 7: Plot n_m versus T_cal for all concentrations ###
    plot_results(conc_t_events, concentrations, results_path)
    

if __name__ == "__main__":
    main()    
