
# Atmospheric and Physical Chemistry - Thomas Koop
+ __Discipline__: Chemistry
+ __GitLab__: https://gitlab.ub.uni-bielefeld.de/conquaire/atmospheric-and-physical-chemistry
+ __Description__: (Atmospheric Ice Nucleation): In the context of the DFG research unit INUIT, the group of Prof. Dr. Thomas Koop carries out ice nucleation experiments with the same type of materials using diverse experimental techniques. CONQUAIRE will support data sharing between all involved groups to evaluate whether these data sets are internally consistent with each other, and finally to combine them, thus providing a parametrization that represents all data from the different techniques.
+ __People__: 
    + __PI__: Prof. Dr. Thomas Koop <thomas.koop@uni-bielefeld.de>
    + __Postdoc__: Carsten Budke <carsten.budke@uni-bielefeld.de>
    + __Doc__: Evelyn Jantsch (@ejantsch), <ejantsch@uni-bielefeld.de>  

----
